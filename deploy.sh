#!/bin/bash
set -e

[[ -z "${AWS_PROFILE}" ]] && { echo "Error: AWS_PROFILE environment variable not set!"; exit 1; }
[[ -z "${AWS_REGION}" ]] && { echo "Error: AWS_REGION environment variable not set!"; exit 1; }
[[ -z "${EKS_CLUSTER_NAME}" ]] && { echo "Error: EKS_CLUSTER_NAME environment variable not set!"; exit 1; }
[[ -z "${KEYNAME}" ]] && { echo "Error: KEYNAME environment variable not set!"; exit 1; }

export EKS_CLUSTER_STACK_NAME="${EKS_CLUSTER_NAME}"
export ADMIN_CIDR="$(dig +short myip.opendns.com @resolver1.opendns.com)/32"
export NODE_GROUP_NAME="${EKS_CLUSTER_NAME}-nodes"
export NODE_GROUP_STACK_NAME="${EKS_CLUSTER_STACK_NAME}-nodes"
export NODE_IMAGE_ID="ami-73a6e20b"

# VPC stack
echo "Creating VPC stack called ${EKS_CLUSTER_STACK_NAME}-vpc.."
aws cloudformation create-stack \
    --stack-name "${EKS_CLUSTER_STACK_NAME}-vpc" \
    --region "${AWS_REGION}" \
    --template-body file://cloudformation/eks-vpc.yaml \
    --parameters \
        ParameterKey=EnvironmentName,ParameterValue="${EKS_CLUSTER_NAME}"

echo "Waiting for the ${EKS_CLUSTER_STACK_NAME}-vpc stack to complete.."
aws cloudformation wait stack-create-complete \
    --stack-name "${EKS_CLUSTER_STACK_NAME}-vpc" \
    --region "${AWS_REGION}"
echo -e "${EKS_CLUSTER_STACK_NAME}-vpc stack complete.\n"

# EKS stack
echo "Creating EKS stack called ${EKS_CLUSTER_STACK_NAME}.."
aws cloudformation create-stack \
    --stack-name "${EKS_CLUSTER_STACK_NAME}" \
    --region "$AWS_REGION" \
    --template-body file://cloudformation/eks-control.yaml \
    --parameters \
        ParameterKey=ClusterName,ParameterValue="${EKS_CLUSTER_NAME}" \
        ParameterKey=VPCStackName,ParameterValue="${EKS_CLUSTER_STACK_NAME}-vpc" \
    --capabilities CAPABILITY_IAM

echo "Waiting for ${EKS_CLUSTER_STACK_NAME} to complete.."
aws cloudformation wait stack-create-complete \
    --stack-name "${EKS_CLUSTER_STACK_NAME}" \
    --region "${AWS_REGION}"
echo -e "${EKS_CLUSTER_STACK_NAME} stack complete.\n"

# EKS worker node stack
echo "Creating EKS worker node group stack called ${NODE_GROUP_STACK_NAME}.."
aws cloudformation create-stack \
    --stack-name "${NODE_GROUP_STACK_NAME}" \
    --region "${AWS_REGION}" \
    --template-body file://cloudformation/eks-nodegroup.yaml \
    --parameters \
        ParameterKey=VPCStackName,ParameterValue="${EKS_CLUSTER_STACK_NAME}-vpc" \
        ParameterKey=ClusterStackName,ParameterValue="${EKS_CLUSTER_STACK_NAME}" \
        ParameterKey=AdminCIDR,ParameterValue="${ADMIN_CIDR}" \
        ParameterKey=KeyName,ParameterValue="${KEYNAME}" \
        ParameterKey=ClusterName,ParameterValue="${EKS_CLUSTER_NAME}" \
        ParameterKey=NodeGroupName,ParameterValue="${NODE_GROUP_NAME}" \
        ParameterKey=NodeImageId,ParameterValue="${NODE_IMAGE_ID}" \
    --capabilities CAPABILITY_IAM

echo "Waiting for ${NODE_GROUP_STACK_NAME} to complete.."
aws cloudformation wait stack-create-complete \
    --stack-name "${NODE_GROUP_STACK_NAME}" \
    --region "${AWS_REGION}"
echo -e "${NODE_GROUP_STACK_NAME} stack complete.\n"

echo -e "All EKS stacks complete!\n"

echo "Writing kubectl config to /tmp/kubectl-config-${EKS_CLUSTER_NAME}"
python3 config/create_kubectl.py > "/tmp/kubectl-config-${EKS_CLUSTER_NAME}"

echo "Writing AWS auth cm to /tmp/aws-auth-cm-${EKS_CLUSTER_NAME}.yaml"
python3 config/create_aws_auth_cm.py > "/tmp/aws-auth-cm-${EKS_CLUSTER_NAME}.yaml"
