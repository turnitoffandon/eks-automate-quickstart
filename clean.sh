#!/bin/bash
set -e

[[ -z "${AWS_PROFILE}" ]] && { echo "Error: AWS_PROFILE environment variable not set!"; exit 1; }
[[ -z "${AWS_REGION}" ]] && { echo "Error: AWS_REGION environment variable not set!"; exit 1; }
[[ -z "${EKS_CLUSTER_NAME}" ]] && { echo "Error: EKS_CLUSTER_NAME environment variable not set!"; exit 1; }

export EKS_CLUSTER_STACK_NAME="${EKS_CLUSTER_NAME}"
export NODE_GROUP_STACK_NAME="${EKS_CLUSTER_STACK_NAME}-nodes"

echo "Deleting EKS worker node group stack called ${NODE_GROUP_STACK_NAME}.."
aws cloudformation delete-stack \
    --stack-name ${NODE_GROUP_STACK_NAME} \
    --region "${AWS_REGION}"

echo "Waiting for ${NODE_GROUP_STACK_NAME} to be deleted.."
aws cloudformation wait stack-delete-complete \
    --stack-name "${NODE_GROUP_STACK_NAME}" \
    --region "${AWS_REGION}"
echo -e "${NODE_GROUP_STACK_NAME} stack deleted.\n"

echo "Deleting EKS stack called ${EKS_CLUSTER_STACK_NAME}.."
aws cloudformation delete-stack \
    --stack-name ${EKS_CLUSTER_STACK_NAME} \
    --region "${AWS_REGION}"
echo "Waiting for ${EKS_CLUSTER_STACK_NAME} to be deleted.."
aws cloudformation wait stack-delete-complete \
    --stack-name "${EKS_CLUSTER_STACK_NAME}" \
    --region "${AWS_REGION}"
echo -e "${EKS_CLUSTER_STACK_NAME} stack deleted.\n"

echo "Deleting VPC stack called ${EKS_CLUSTER_STACK_NAME}-vpc.."
aws cloudformation delete-stack \
    --stack-name "${EKS_CLUSTER_STACK_NAME}-vpc" \
    --region "${AWS_REGION}"
echo "Waiting for ${EKS_CLUSTER_STACK_NAME}-vpc to be deleted.."
aws cloudformation wait stack-delete-complete \
    --stack-name "${EKS_CLUSTER_STACK_NAME}-vpc" \
    --region "${AWS_REGION}"
echo -e "${EKS_CLUSTER_STACK_NAME}-vpc stack deleted.\n"

echo "EKS stacks deleted."