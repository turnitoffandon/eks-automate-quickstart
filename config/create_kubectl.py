#!/usr/bin/env python3
'''
Creates aws-auth ConfigMap. When applied to the EKS cluster it will allow the worker nodes to join the cluster.
'''
import os
import boto3
import jinja2

def get_env_var(env_var):
    ''' Get the value of specified environment variable. Error if not set '''
    env_var_value = os.environ.get(env_var)
    if env_var_value is None:
    	print("ERROR: %s environment variable is not set!" % env_var)
    	exit (1)
    else:
    	return env_var_value

def get_eks_cluster_details(aws_region, eks_cluster_name):
    ''' Get the EKS cluster role ARN '''
    client = boto3.client('eks', region_name=aws_region)
    response = client.describe_cluster(name=eks_cluster_name)
    cluster_details = response['cluster']

    return cluster_details

def main():
    ''' main '''
    aws_profile = get_env_var("AWS_PROFILE")
    aws_region = get_env_var("AWS_REGION")
    eks_cluster_name = get_env_var("EKS_CLUSTER_NAME")

    eks_cluster_api_endpoint = get_eks_cluster_details(aws_region, eks_cluster_name)['endpoint']
    eks_cluster_ca = get_eks_cluster_details(aws_region, eks_cluster_name)['certificateAuthority']['data']

    template_loader = jinja2.FileSystemLoader(searchpath=["./","./config/"])
    template_env = jinja2.Environment(loader=template_loader)
    template_file = "kubeconfig.j2"
    template = template_env.get_template(template_file)
    output_text = template.render(eks_cluster_name=eks_cluster_name,
                                  aws_profile = aws_profile,
                                  eks_cluster_ca = eks_cluster_ca,
                                  eks_cluster_api_endpoint= eks_cluster_api_endpoint)

    print (output_text)

if __name__ == '__main__':
    main()