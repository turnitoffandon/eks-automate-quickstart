#!/usr/bin/env python3
'''
Creates aws-auth ConfigMap. When applied to the EKS cluster it will allow the worker nodes to join the cluster.
'''
import os
import boto3
import jinja2

def get_env_var(env_var):
    ''' Get the value of specified environment variable. Error if not set '''
    env_var_value = os.environ.get(env_var)
    if env_var_value is None:
    	print("ERROR: %s environment variable is not set!" % env_var)
    	exit (1)
    else:
    	return env_var_value

def get_cf_stack_output_value(aws_region, stack_name, output_key):
    client = boto3.client('cloudformation', region_name=aws_region)
    response = client.describe_stacks(StackName=stack_name)
    cfnOutput = response['Stacks'][0]['Outputs']
    for i in cfnOutput:
        if i['OutputKey'] == output_key:
            value = i['OutputValue']
        else:
            continue
    return value

def main():
    ''' main '''
    aws_profile = get_env_var("AWS_PROFILE")
    aws_region = get_env_var("AWS_REGION")
    node_group_stack_name = get_env_var("NODE_GROUP_STACK_NAME")

    eks_node_role_arn = get_cf_stack_output_value(aws_region, node_group_stack_name, "NodeInstanceRole")

    template_loader = jinja2.FileSystemLoader(searchpath=["./","./config/"])
    template_env = jinja2.Environment(loader=template_loader)
    template_file = "aws-auth-cm.yaml.j2"
    template = template_env.get_template(template_file)
    output_text = template.render(rolearn=eks_node_role_arn)

    print (output_text)

if __name__ == '__main__':
    main()