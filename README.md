# EKS Automated Quickstart

Code to automate the steps in Amazon's [Getting Started with Amazon EKS](https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html) guide. This includes:

1. **EKS Cluster**: Amazon EKS control plane which consists of the nodes which run the Kubernetes software (etcd and the Kubernetes API server). Note that the EKS cluster runs in AWS managed accounts and the API is exposed via an Amazon EKS endpoint.

2. **Autoscaling Group of EKS worker nodes**: EKS worker nodes run as EC2 instances running the EKS optimised AMI in your AWS account.

3. **IAM Roles and policies**: IAM roles and policies required to run the EKS cluster.

Details on how to use the code to create the resources can be found here: [https://turnitoffandon.xzy/aws/2018/07/23/eks-automated-quickstart.html](https://turnitoffandon.xzy/aws/2018/07/23/eks-automated-quickstart.html)

## Meta

Matt Wolstencroft - turnitoffandon@pm.me

[https://bitbucket.org/turnitoffandon](https://bitbucket.org/turnitoffandon)

## Contributing

1. Fork it (<https://github.com/yourname/yourproject/fork>)
2. Create your feature branch (`git checkout -b feature/fooBar`)
3. Commit your changes (`git commit -am 'Add some fooBar'`)
4. Push to the branch (`git push origin feature/fooBar`)
5. Create a new Pull Request
